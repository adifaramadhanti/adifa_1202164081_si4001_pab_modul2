package com.example.adifa_1202164081_si4001_pab_modul2;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Summary extends AppCompatActivity {
    private TextView tujuaned, berangkated, pulanged, jumlahtiketed, biayaed;
    private int tujuan, jam, menit, biaya, jmlTiket;
    private String tanggalpulang, tanggal = "tanggal", nama_tujuan;
    private String[] tujuansemua = {"Jakarta", "Cirebon", "Bekasi"};
    private boolean switch1 = false;

    @Override
    protected void onCreate(Bundle savedInstanceBundle) {
        setTheme(R.style.AppTheme);
        Log.d("Summary", "onCreate di load, bosss!!");
        Intent intent = getIntent();
        Bundle sum = intent.getExtras();
        super.onCreate(savedInstanceBundle);
        setContentView(R.layout.activity_summary);
        Log.d("Summary - Bundle", "Bundle diterima!");
        if (sum != null) {
            tujuan = sum.getInt("tujuan", 0);
            jmlTiket = sum.getInt("jmlTiket", 0);
            jam = sum.getInt("jam", 0);
            menit = sum.getInt("menit", 0);
            biaya = sum.getInt("biaya", 0);
            switch1 = sum.getBoolean("switche", false);
            if (switch1) {
                tanggalpulang = sum.getString("tanggalpulang", "null");
            }
            tanggal = sum.getString("tanggal", "null");
            Log.d("Summary - Bundle", "Bundle dibuka!");
        }

        //Mengatur nama tujuan
        tujuaned = findViewById(R.id.tujuan_ed);
        switch (tujuan) {
            case 0:
                nama_tujuan = tujuansemua[0];
                tujuaned.setText(nama_tujuan);
                break;
            case 1:
                nama_tujuan = tujuansemua[1];
                tujuaned.setText(nama_tujuan);
                break;
            case 2:
                nama_tujuan = tujuansemua[2];
                tujuaned.setText(nama_tujuan);
                break;
        }

        //Mengatur tanggal
        berangkated = findViewById(R.id.berangkat_ed);
        berangkated.setText(tanggal);
        if (switch1) {
            pulanged = findViewById(R.id.pulang_ed);
            pulanged.setText(tanggalpulang);
        } else {
            pulanged = findViewById(R.id.pulang_ed);
            TextView txtpulang = findViewById(R.id.txt_pulang);
            pulanged.setVisibility(View.GONE);
            txtpulang.setVisibility(View.GONE);
        }

        //Mengatur jumlah tiket
        jumlahtiketed = findViewById(R.id.jmltiket_ed);
        jumlahtiketed.setText(Integer.toString(jmlTiket));

        //Mengatur total biaya
        biayaed = findViewById(R.id.biaya_ed);
        biayaed.setText(getString(R.string.hartot_txt) + Integer.toString(biaya));
        Log.d("Summary-onCreate", "sabar masih onCreate");
    }

    public void konfirm(View view) {
        Log.d("Summary-konfirm", "konfirm dimulai");
        Toast.makeText(this, getString(R.string.toast_thanks), Toast.LENGTH_LONG)
                .show();
        Intent pulangkanlah = new Intent();
        pulangkanlah.putExtra("newsaldo", Integer.toString(biaya));
        setResult(Activity.RESULT_OK, pulangkanlah);
        Log.d("Summary-konfirm", "Finish!");
        finish();
    }
}
